import 'bootstrap/dist/css/bootstrap.css';
import '@fortawesome/fontawesome-free/css/all.css';
import angular from 'angular';
import $ from 'jquery';

import './index.scss';

const app = angular.module('app', [ 
   require('@uirouter/angularjs').default
]);

//COMPONENTS
app.component('myForm', require('./components/form/form.cmp').FormComponent);
app.component('myFormSteps', require('./components/form/steps/steps.cmp').FormStepsComponent);
app.component('myInput', require('./components/my-input/my-input.cmp'));
app.component('myRadio', require('./components/my-radio/my-radio.cmp'));

//DIRECTIVES
app.directive('myStepper', require('./directives/my-stepper/my-stepper.directive'));

app.config([ '$stateProvider', '$urlRouterProvider',
    ($stateProvider, $urlRouterProvider) => {

    $urlRouterProvider.otherwise('/form/steps/one');
  
    $stateProvider.state('form', {
        url: '/form/',
        component: 'myForm',
    });

    $stateProvider.state('form.steps', {
        url: 'steps/:step',
        component: 'myFormSteps',
        resolve: {
            step: [ '$transition$', ($transition$) => $transition$.params().step ],
            to: [ 
                '$urlService', 
                ($urlService) => {
                    return (step) => $urlService.url('/form/steps/' + step);
                }

            ]
        }
    });
}]);

app.run([ '$rootScope', ($rootScope) => {
 //...
}]);

$(() => angular.bootstrap(document, [ 'app' ]));



