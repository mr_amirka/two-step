
import './my-input.cmp.scss';

module.exports = {
	bindings: {
        ngModel: '=',
        ngInvalid: '<',
        ngChange: '&',
        label: '@'
    },
    template: require('./my-input.cmp.html')
};

