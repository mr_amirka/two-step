import './form.cmp.scss';
import { steps$ } from './steps/steps.cmp';

export const FormComponent = {
    template: require('./form.cmp.html'),
    controller() {
    	const subscription = steps$.subscribe((steps) => {
    		this.steps = steps;
    	});
    	this.$onDestroy = () => {
    		subscription.unsubscribe();
    	};
	}
};

