
import { BehaviorSubject } from 'rxjs';

import * as validator from '../../../utils/validator';

export const steps$ = new BehaviorSubject([]);

const storageKey = 'form-steps';
const parse = (value) => {
    try {
        return JSON.parse(value) || {};
    } catch(e){}
    return {};
};

export const form$ = new BehaviorSubject(parse(localStorage.getItem(storageKey)));
form$.subscribe((form) => localStorage.setItem(storageKey, JSON.stringify(form)));

const steps = [ 'one', 'two' ];
const forms = {
	one: {
        label: 'Step one',
        validators: {
            fullname: validator.invalidateTextProvider(true, 2, 255),
            age: validator.invalidateNumberProvider(false, 0, 1000, true),
            sex: validator.invalidateTextProvider(true)
        }
    },
	two: {
        label: 'Step two',
        validators: {
            phone: validator.invalidatePhoneProvider(true),
            email: validator.invalidateEmailProvider(true)
        }
    }
};

export const FormStepsComponent = {
    template: require('./steps.cmp.html'),
    bindings: {
    	step: '<',
    	to: '<'
    },
    controller: [ '$timeout', function($timeout) {
        let $$validators;
        let $$stepIndex;
        const subscription = form$.subscribe((form) => this.form = form);

        this.invalids = {};
        this.$onDestroy = () => {
            subscription.unsubscribe();
        };

    	this.$onInit = () => {
    		const step = this.step;
    		$$stepIndex = steps.indexOf(step);
    		if ($$stepIndex < 0) {
    			this.to(steps[0]);
    			return;
    		}
            const currentStepIndex = getCurrentStepIndexByFollow($$stepIndex);
            if (currentStepIndex !== $$stepIndex) {
                this.to(steps[currentStepIndex]);
                return;
            }
            $$validators = forms[step].validators;
	      	stepsUpdate();
	    };
    	
    	this.next = (_next) => {
    		const step = steps[$$stepIndex + (_next || 1)];
            step && this.to(step);
    	};

        this.update = () => $timeout(() => {
            stepsUpdate();
            form$.next({ ...this.form });
        });
       
        this.reset = () => {
            form$.next({});
            stepsUpdate();
            this.to(steps[0]);
        };

        const getCurrentStepIndexByFollow = (stepIndex) => {
            for (let i = 0; i < stepIndex; i++) {
                if (isInvalidStep(steps[i])) return i;
            }
            return stepIndex;
        };
        const isInvalidStep = (step) => {
            const form = this.form;
            const validators = forms[step].validators;
            for (let k in validators) {
                if (validators[k](form[k])) return true;
            }
        };
        const invalidate = () => {
            const form = this.form;
            const invalids = this.invalids;
            let hasInvalid = false;
            for (let k in $$validators) {
                if (invalids[k] = $$validators[k](form[k])) {
                    hasInvalid = true;
                }
            }
            this.hasInvalid = hasInvalid;
        };

    	const stepsUpdate = () => {
            invalidate();
            steps$.next(steps.map((stepName, stepIndex) => {
                return {
                    label: forms[stepName].label,
                    active: stepIndex <= $$stepIndex,
                    valid: !isInvalidStep(stepName)
                };
            }));
    	};

	} ]
    
};

