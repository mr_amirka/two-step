import './my-radio.cmp.scss';

module.exports = {
	bindings: {
        ngModel: '=',
        label: '@',
        name: '@',
        values: '<',
        ngInvalid: '<',
        ngChange: '&'
    },
    template: require('./my-radio.cmp.html')
};

