import './my-stepper.directive.scss';

module.exports = () => {
  	return { 
	    restrict: 'E',
	    scope: {
	    	steps: '<'
	    },
	    template: require('./my-stepper.directive.html'),
	    link(scope, element, attrs) {
		    scope.position = (index) => {
		    	const length = scope.steps.length;
		    	return length < 2 ? 0 : index * 100 / (length - 1);
		    };
		    scope.percent = () => {
		    	const steps = scope.steps;
		    	const length = steps.length;
		    	if (length < 2) return 0;
		    	let index = 0;
		    	for (let i = 0; i < length; i++) {
		    		if (steps[i].active) index = i;
		    	}
		    	return index * 100 / (length - 1);
		    };
		}
  	};
};