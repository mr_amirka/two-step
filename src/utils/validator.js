export const invalidateTextProvider = (hasRequire, min, max) => {
	hasRequire = !!hasRequire;
	min || (min = 0);
	const hasMax = typeof max === 'number';
	const invalidate = (input) => {
		const length = String(input).length;
		return length < min || hasMax && length > max;
	};
	return (input) => input ? invalidate(input) : hasRequire;
};

export const invalidateNumberProvider = (hasRequire, min, max, hasOnlyInt) => {
	hasRequire = !!hasRequire;
	const hasMin = typeof min === 'number';
	const hasMax = typeof max === 'number';
	const invalidate = (input) => {
		if (invalidateNumber(input)) return true;
		const value = parseFloat(input);
		return isNaN(value)
			|| hasOnlyInt && (value - Math.floor(value)) !== 0
			|| hasMin && min > value 
			|| hasMax && max < value;
	};
	return (input) => input ? invalidate(input) : hasRequire;
};
export const invalidateNumber = (input) => !reNumber.test(input);
const reNumber = /^\d+(\.\d+)?$/;

// примитивная проверка
export const invalidatePhoneProvider = (hasRequire) => {
	hasRequire = !!hasRequire;
	return (input) => input ? invalidatePhone(input) : hasRequire;
};
export const invalidatePhone = (input) => !rePhone.test(input);
const rePhone = /^\+?[- ()\d]+$/;

export const invalidateEmailProvider = (hasRequire) => {
	hasRequire = !!hasRequire;
	return (input) => input ? invalidateEmail(input) : hasRequire;
};
export const invalidateEmail = (input) => !reEmail.test(String(input).toLowerCase());
const reEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;   
